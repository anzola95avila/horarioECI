# http://estudiantes.escuelaing.edu.co/estudiantes/HorarioAsigEst?iden=arsw&submit=Buscar
# ================

import os
import sys
import argparse
import time
import datetime
from threading import Thread
from playsound import playsound # pip install playsound
import requests # pip install requests
from bs4 import BeautifulSoup # pip install beautifulsoup4
from unidecode import unidecode # pip install unidecode

# DEFAULT_ADDRESS = "http://estudiantes.escuelaing.edu.co/estudiantes/HorarioAsigEst"
DEFAULT_ADDRESS = "http://190.24.150.76:81/estudiantes/HorarioAsigEst"
AUDIO_FILE = "./error.wav"
global verbose
global annoy

class GetException(Exception):
    """
    Exception designated when it's not posible to get the web page
    """

def lprint(*args, **kwargs):
    """
    makes print for log
    """
    global verbose
    if verbose:
        print("[LOG {}] ".format(get_timestamp()), end="")
        print(*args, **kwargs)

def eprint(*args2, **kwargs2):
    """
    makes print to stderr
    """
    print("[ERROR {}] ".format(get_timestamp()), end="", file=sys.stderr)
    print(*args2, file=sys.stderr, **kwargs2)

def concurrent_function_call(function, arguments = ()):
    """
    makes a function call concurrently
    function: callable function
    arguments: arguments to be passed to the function
    """
    lprint("made a concurrent call of function \'{}\' with args \'{}\'".format(str(function), arguments))
    thread = Thread(target = function, args = arguments)
    thread.start()
    
def send_notification(msg, title=None):
    """
    sends a OS notification about something
    msg: str message
    title: str (None by default)
    """
    assert title is None or isinstance(title, str)
    assert isinstance(msg, str)

    title = title if title is not None else "CHANGES"

    if sys.platform == "linux":
        concurrent_function_call(os.system, ("notify-send -u critical \'{}\' \'{}\'".format(title, msg) ,))
    elif sys.platform == "darwin":
        concurrent_function_call(os.system, ("osascript -e 'display notification \"{1}\" with title \"{0}\"'".format(title, msg), ))
    elif sys.platform == "win32":
        concurrent_function_call(os.system, ("msg * \"{}: {}\"".format(title, msg),))

def get_page_info(address, subject):
    """
    return a tuple with the table index and a list of dictionaries with key:value
    where key corresponds to the value in index
    address: str
    subject: str
    """
    assert isinstance(address, str), type(address)
    assert isinstance(subject, str), type(subject)

    payload = {"iden" : subject.lower()}
    try:
        page = requests.get(address, params = payload)
    except Exception as error:
        eprint("ERROR: error getting the address \'{}\'".format(address))
        eprint(str(error))
        raise GetException
    
    soup = BeautifulSoup(page.text, 'html.parser')

    table = soup.find(class_='texto')
    try:
        rows = table.find_all('tr')
    except Exception as error:
        eprint("ERROR: error getting \'{}\' subject".format(subject))
        eprint(str(error))
        raise GetException

    index = [unidecode(tag.string).lower() for tag in rows[0].find_all("td")]
    
    table_list = []
    for row in rows[1:]:
        row_dict = {}
        tags = row.find_all("td")
        for i in range(len(tags)):
            row_dict[index[i]] = tags[i].string

        table_list.append(row_dict)

    return (index, table_list)

def filter_data(table, filters):
    """
    returns the data filtered
    table: list of dicts with data
    filters: dictionary with key:value where 
             key = value to filter
             value = a list of values
    """
    assert isinstance(table, list)
    assert all([isinstance(k, dict) for k in table])
    assert isinstance(filters, dict)
    # assert all([isinstance(k, function) for k in filters.values()])

    result = []
    for row in table:
        assert all([(fk in row.keys()) for fk in filters.keys()])
        
        is_valid = all([filters.get(fil, lambda x: True)( row.get(fil) ) for fil in filters.keys()])
        if is_valid:
            result.append(row)

    return result

def gen_filters(groups, professors):
    """
    returns a dict with the key of the filter and a value with a callable function
    that receives one str argument
    groups: list with groups
    professors: list with professors
    """
    filters = dict()
    
    if len(groups) > 0 and groups[0].strip() != "":
        filters['grupo'] = lambda x: x in groups
        
    if len(professors) > 0 and professors[0].strip() != "":
        filters['profesor'] = lambda x: any([all([p.lower() in unidecode(x).lower() for p in prof.split("_")])
                                             for prof in professors])

    return filters

def command_line_parser():
    """
    returns a dict with the options passed to the command line
    according with the options available
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("-a", "--address", type=str, default=DEFAULT_ADDRESS,
                        help="page address (default: {})".format(DEFAULT_ADDRESS))

    parser.add_argument("-p", "--professors", type=str, default="",
                        help="professors comma separated")

    parser.add_argument("-g", "--groups", type=str, default="",
                        help="group number's comma separated")

    parser.add_argument("-t", "--delta", type=float, default=2.0,
                        help="time interval in seconds between each check (default=2s)")

    parser.add_argument("-c", "--criteria", type=str, default="s",
                        help="criteria to compare, i.e. what has changed?\n"
                        + "(a = space available, p = professor, s = has space, "
                        + "S = has space now(don't consider previous value), d = space is different)\n"
                        + "i.e. -c ap -> will compare if space or professor changed; "
                        + "default: s")

    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--annoy", action="store_true",
                        help="play an annoying sound in each notification")
    
    parser.add_argument("subjects", type=str,
                        help="subjects to check comma separated")

    args = parser.parse_args()

    options = dict()
    options['address']    = args.address
    options['professors'] = args.professors
    options['groups']     = args.groups
    options['delta']      = args.delta
    options['subjects']   = args.subjects
    options['criteria']   = args.criteria
    options['verbose']    = args.verbose
    options['annoy']      = args.annoy
    assert args.delta > 0.0, "can't be less or equal to 0"
    assert all([cr in "apsSd" for cr in args.criteria]), "{} is not a valid criteria".format(args.criteria)

    return options

def gen_criteria(criteria_str):
    """
    returns a dict with a function of two(2) arguments that defines if something
    has changed according to a criteria
    criteria_str: str with criteria
    """

    criteria = dict()
    if 'd' in criteria_str:
        tag = 'disponible'
        criteria[tag] = lambda x,y: x != y
    
    if 'a' in criteria_str:
        tag = 'disponible'
        criteria[tag] = lambda x,y: int(x) < int(y)

    if 'p' in criteria_str:
        tag = 'profesor'
        criteria[tag] = lambda x,y: x != y

    if 's' in criteria_str:
        tag = 'disponible'
        criteria[tag] = lambda x,y: (int(x) <= 0 < int(y))

    if 'S' in criteria_str:
        tag = 'disponible'
        criteria[tag] = lambda x,y: int(y) > 0

    return criteria
    

def compare_data(previous, current, criteria):
    """
    compares data and returns the differences between 'previous' and 'current'
    Assumes that the rows from previous and current haven't changed.

    previous: dict with key:subject and value:list with data
    current: dict with key:subject and value:list with data
    criteria: dict with functions to determine if data is different
    """
    assert previous is None or isinstance(previous, dict), "{}".format(previous)
    if previous is None:
        return dict()
    assert all([isinstance(i, list) for i in previous.values()])
    assert isinstance(current, dict), "{}".format(current)
    assert all([isinstance(i, list) for i in current.values()])

    assert len(previous) == len(current)

    diffs = dict()
    
    for subject in current.keys():
        diffs[subject] = diffs.get(subject, [])
        size = len(current[subject])
        for i in range(size):
            diff_criteria = []
            for tag in criteria.keys():
                if criteria[tag](previous[subject][i][tag], current[subject][i][tag]):
                    diff_criteria.append(tag)

            if len(diff_criteria) > 0:
                diffs[subject].append( (previous[subject][i], current[subject][i], diff_criteria) )

    return diffs

def get_timestamp() -> str:
    """
    returns current timestamp in a str
    """
    now = datetime.datetime.now()
    return "{}/{}/{} {}:{}:{}".format(now.year,
                                      now.month,
                                      now.day,
                                      now.hour,
                                      now.minute,
                                      now.second)

def notify_diffs(diffs):
    """
    notifies through a log and a notification to the OS about the difference
    in data
    returns: None
    diffs: dict with lists with 3-tuples, with previous data, current data and names of criterias
    """
    assert isinstance(diffs, dict)
    assert all([isinstance(i, list) and
                all([isinstance(l, tuple) and len(l) == 3 for l in i])
                for i in diffs.values()])

    for subject in diffs.keys():
        msg = ""
        for i in range(len(diffs[subject])):
            values = diffs[subject][i]
            criterias = values[2]
            for cr in criterias: # criterias
                msg += "\n\t{0} has changed (group {3}): from {1} to {2}".format(cr,
                                                                                 values[0][cr],
                                                                                 values[1][cr],
                                                                                 values[1]['grupo'])
        title = subject.upper()

        if msg != "":
            print("[{2}] {0}: {1}".format(title, msg, get_timestamp()))
            send_notification(msg, title)
            play_annoy_sound()

def play_annoy_sound():
    global annoy
    def sound():
        try:
            playsound(AUDIO_FILE)
        except Exception as error:
            eprint("Could not reproduce annoying audio file\n" + str(error))
    
    if annoy:
        concurrent_function_call(sound)
                    
def main():
    global verbose, annoy
    
    options = command_line_parser()
    
    address    = options['address']
    subjects   = options['subjects'].split(',')
    groups     = options['groups'].split(',')
    professors = options['professors'].split(',')
    delta      = options['delta']
    criteria   = options['criteria']
    verbose    = options['verbose']
    annoy      = options['annoy']

    filters = gen_filters(groups, professors)

    criteria_dict = gen_criteria(criteria)
    previous_data = None
    current_data = None

    print("Starting app at {}".format(get_timestamp()))
    
    while True:
        lprint("================================================================")

        current_data = dict()

        n_subjects, i = len(subjects), 0
        while i < n_subjects:
            subject = subjects[i]
            lprint("++++++++++++++++")
            lprint(subject.upper())

            try:
                table = get_page_info(address, subject)
            except GetException:
                eprint("error getting the subject, checking all subjects again")
                i = 0 # resets the counter so it checks again all subjects
                time.sleep(delta)
                continue
            filtered_data = filter_data(table[1], filters)

            current_data[subject] = filtered_data
            
            lprint(filtered_data)
            i += 1

        diffs = compare_data(previous_data, current_data, criteria_dict)
        notify_diffs(diffs)
        
        previous_data = current_data
        time.sleep(delta)
        
    
if __name__ == "__main__":
    main()
